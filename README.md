Official page:

  https://flexmojos.atlassian.net/wiki/spaces/FLEXMOJOS/overview

Set path to Maven:

    execute vim ~/.bash_profile
    add path (for example export PATH=$PATH:/Users/admin/Downloads/apache-maven-3.5.0/bin)
    save and exit 

Set path to FlashPlayer for macOS in maven settings(/Users/admin/Downloads/apache-maven-3.5.0/conf/settings.xml and /Users/admin/.m2/settings.xml):
    
    <profile>
      <id>test</id>
      <properties>
        <flex.flashPlayer.command>/Applications/Adobe Animate CC 2018/Players/Flash Player.app/Contents/MacOS/Flash Player</flex.flashPlayer.command>
      </properties>
    <profile>
